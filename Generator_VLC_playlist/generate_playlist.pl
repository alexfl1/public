#!/usr/bin/perl 
#
#  Generate VLC xspf playlist
#  2018-01-23
#   
# RUN:
# ./preprocessing.sh  List_of_channals.csv  | ./generate_playlist.pl
#
# Format of STDIN:
# <GROUP>:<PORT><Space><Name of the channel>
# Example:
# 239.170.170.30:58631   Eurosport 1 HD



print <<EOT;
<?xml version="1.0" encoding="UTF-8"?>
<playlist xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/" version="1">
EOT

print "<title>"."Default Playlist"."</title>\n";
print "<trackList>\n";
	
my $count=0;
for (<>)	{
	chomp;
	my @s=split(/\s+/,$_);
	my $group_port =  shift @s;
	my $name = join ' ',@s;
	my $num = $count+1;
		print "<track>\n";
		print "<location>udp://@".$group_port."</location>\n";
		print "<title>".$num.": ".$name." (".$group_port.")</title>\n";
		print "<extension application=".'"'."http://www.videolan.org/vlc/playlist/0".'"'."> \n";
		print "<vlc:id>".$count."</vlc:id>\n";
		print "</extension>\n";
		print "</track>\n";
	$count++;
}

print "</trackList>\n";
print "<extension application=".'"'."http://www.videolan.org/vlc/playlist/0".'"'."> \n";
my $i=0;
while ($i<$count) {
	print '<vlc:item tid="'.$i.'"/>'."\n";
	$i++;
}
print "</extension>\n";
print "</playlist>\n";
