#!/bin/bash

# $1 = Name of CSV file 
#
# The task of the programm:
# Print 1 and 8 coumns of CSV file

if [ $# != 1 ]; then
echo "Please give name of CSV file"
echo "For run:"
echo "./preprocessing.sh  List_of_channals.csv"
exit 1
fi

if [ -f $1 ]; then

cat $1 | awk -F, '{print $8, " ",$1}'

else
echo "File is not found"
exit 2
fi
